import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize, retryWhen } from 'rxjs/operators';

import { Facility } from '../_models/facility';
import { Param } from '../_models/param';

const facilities: Facility[] = [
    {
        id: 1,
        name: "Аэродинамическая установка с высокочастотным индукционным нагревом газа (У-13 ВЧП)",
        description: "Исследование теплообмена в высокотемпературных химически чистых  " +
            "газовых потоках и испытания теплозащитных материалов",
        startDate: new Date(1982, 0, 1),
        staffNumber: -1
    },
    {
        id: 2,
        name: "КВАНТ-20",
        description: "Вакуумно-тепловые испытания оптической аппаратуры",
        startDate: new Date(1995, 0, 1),
        staffNumber: -1
    },
    /*{
        id: 3,
        name: "Реверберационная акустическая камера ВАУРК-130",
        description: "Акустические испытания на прочность",
        type: Facility.Type.AccousticFacility,
        organizationId: 3,
        operationStatus: Facility.OperationStatus.Active,
        startDate: new Date(1995, 0, 1),
        staffNumber: -1,
        engineersNumber: -1,
        techniciansNumber: -1,
        workersNumber: -1,
        averageStaffAge: -1,
        cost: -1,
        currentCost: -1,
        annualAmount: -1,
    },
    {
        id: 4,
        name: "Электродинамический вибростенд V964LS/MPA64",
        description: "Испытание изделий на вибропрочность",
        type: Facility.Type.StrengthFacility,
        organizationId: 3,
        operationStatus: Facility.OperationStatus.Active,
        startDate: new Date(1986, 0, 1),
        staffNumber: -1,
        engineersNumber: -1,
        techniciansNumber: -1,
        workersNumber: -1,
        averageStaffAge: -1,
        cost: -1,
        currentCost: -1,
        annualAmount: -1,
    }*/
];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, params, body } = request;

        // Wrap in delayed observable to simulate server api call.
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // Call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648).
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {
                case url.endsWith('/api/facilities') && method === 'GET':
                    return getFacilities();

                default:
                    // Pass through any requests not handled above.
                    return next.handle(request);
            }
        }

        // ===================================
        // Route functions.
        // ===================================

        function getFacilities(): Observable<HttpResponse<Facility[]>> {
            return ok(facilities);
        }

        // ===================================
        // Helper functions.
        // ===================================

        function ok<T>(body: T): Observable<HttpResponse<T>> {
            return of(new HttpResponse({ status: 200, body }));
        }
    }
}