export interface Facility {
    id: number;
    /** Наименование установки. */
    name: string;
    /** Текстовое описание. */
    description: string;
    /** Огранизация - владелец. */
    organizationName?: string;
    /** Дата начала функционирования. */
    startDate: Date;
    /** Численность персонала по штату. */
    staffNumber: number;
}