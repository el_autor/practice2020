export interface Param<T> {
    /** Имя параметра. */
    name: string;
    /** Короткое обозначение. */
    designation?: string;
    /** Значение параметра. */
    value: T;
    /** Единицы измерения. */
    mUnits?: string;
}