import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Facility } from '../_models/facility';

@Injectable({
  providedIn: 'root'
})
export class FacilitiesService {
  constructor(
      private http: HttpClient,
      @Inject('BASE_PATH') private basePath: string,
  ) { }

  getFacilities(): Observable<Facility[]> {
    return this.http.get<Facility[]>(this.basePath + "api/facilities");
  }
}