import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Param } from '../_models/param';

@Injectable({
  providedIn: 'root'
})
export class ParamsService {
  constructor(
      private http: HttpClient,
      @Inject('BASE_PATH') private basePath: string,
  ) { }

  getParams(): Observable<Param<any>[]> {
    return this.http.get<Param<any>[]>(this.basePath + `api/params`);
  }
}