import { Component, OnInit } from '@angular/core';
import data from '../../assets/js/storage/facility_data.json'
import { FacilitiesService } from '../_services/facilities.service';
import { Facility } from '../_models/facility';
import { Observable } from 'rxjs';
declare const renderAdminka: any;

@Component({
  selector: 'app-adminka',
  templateUrl: 'adminka.component.html',
  styleUrls: ['adminka.component.css']
})
export class AdminkaComponent implements OnInit {
  public facilities$: Observable<Facility[]>;

  constructor(
    private facilitiesService: FacilitiesService
  ) { }

  ngOnInit() {
    this.facilities$ = this.facilitiesService.getFacilities();
  }

  render()
  {
    renderAdminka(data);
  }

}
