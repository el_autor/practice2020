import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminkaComponent } from './adminka/adminka.component';
import { ParametersComponent } from './parameters/parameters.component';
import { AddingComponent } from './adding/adding.component';


const routes: Routes = [
  {
    path: 'adminka',
    component: AdminkaComponent
  },
  {
    path: 'add',
    component: AddingComponent
  },
  {
    path: 'all',
    component: ParametersComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
