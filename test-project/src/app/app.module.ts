import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminkaComponent } from './adminka/adminka.component';
import { ParametersComponent } from './parameters/parameters.component';
import { AddingComponent } from './adding/adding.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { environment } from 'src/environments/environment';
import { FakeBackendInterceptor } from './_helpers/fake-backend';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

@NgModule({
   declarations: [
      AppComponent,
      AdminkaComponent,
      ParametersComponent,
      AddingComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      NbThemeModule.forRoot({ name: 'default' }),
      NbLayoutModule,
      NbEvaIconsModule,
      HttpClientModule
   ],
   providers: [
      {
         provide: 'BASE_PATH',
         useValue: environment.apiUrl
      },
      {
         provide: HTTP_INTERCEPTORS,
         useClass: FakeBackendInterceptor,
         multi: true
     }
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
