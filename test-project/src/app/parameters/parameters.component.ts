import { Component, OnInit } from '@angular/core';
declare const addFacility: any;
declare const changeEditability: any;
declare const renderParameters: any;

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.css']
})
export class ParametersComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  render()
  {
    renderParameters();
  }

  onClickAddButton()
  {
    addFacility();
  }

  onClickEditButton()
  {
    changeEditability();
  }

}
