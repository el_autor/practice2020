function renderAdminka(data)
{
    if (!this.calledBefore)
    {
        this.calledBefore = true;

        var body = document.getElementById("canvas");
        body.style.position = "absolute";
        body.style.left = "0px";
        body.style.top = "0px";
        body.style.width = "100%";
        body.style.height = "100%";

        var table = document.getElementById("facilityTable");        
        table.style.position = "absolute";
        table.style.left = "30%";
        table.style.top = String(window.innerHeight * 0.2) + "px";
        table.style.width = "40%";

        var tableTitle = document.getElementById("tableTitle");
        tableTitle.style.captionSide = "top";
        tableTitle.style.textAlign = "center";
        tableTitle.innerHTML = tableTitle.innerHTML.bold();
        tableTitle.style.border = "1px solid #E5E8E8";

        var verifyButtonGroup = document.getElementById("verifyButtonGroup");
        verifyButtonGroup.style.position = "absolute";
        verifyButtonGroup.style.left = String(table.offsetLeft + table.offsetWidth) + "px";
        verifyButtonGroup.style.top = String(table.offsetTop + 2 * document.getElementById("firstRow").offsetHeight) + "px";
        verifyButtonGroup.style.width = String(table.offsetWidth / 20) + "px";

        for (i = 0; i < data.facilities.length; i++)
        {
            var newRow = table.insertRow(table.rows.length);
            var number = newRow.insertCell(0)
            number.innerHTML = String(i + 1);
            newRow.style.textAlign = "center";
            var name = newRow.insertCell(1);
            name.innerHTML = data.facilities[i].name;

            var newVerifyButton = document.createElement("button");
            newVerifyButton.className = "btn btn-light";
            newVerifyButton.style.width = String(Number(table.offsetWidth) / 20) + "px";
            newVerifyButton.style.height = String(Number(document.getElementById("firstRow").offsetHeight) + 1) + "px";
            newVerifyButton.style.backgroundColor = "yellow";
            newVerifyButton.style.borderColor = "yellow";
            newVerifyButton.style.padding = "0px 0px 0px 0px";
            newVerifyButton.style.textAlign = "center";
            newVerifyButton.innerHTML = "✎".bold();
            newVerifyButton.onclick = setCallback(i, data.facilities[i]);
            document.getElementById("verifyButtonGroup").appendChild(newVerifyButton);        
        }

        var paramsTable = document.getElementById("paramsTable");
        paramsTable.style.position = "absolute";
        paramsTable.style.left = table.style.left;
        paramsTable.style.top = String(table.offsetTop + table.offsetHeight) + "px";
        paramsTable.style.width = table.style.width;

        var title = document.getElementById("paramsTableTitle");
        title.style.captionSide = "top";
        title.style.textAlign = "center";
        title.innerHTML = "Установка".bold();
        title.style.border = "1px solid #E5E8E8";

        paramsTable.style.visibility = "hidden";

    }
    else
    {

    }
}

function setCallback(index, facility)
{
    return function openFacilityParams()
    {
        console.log(index);

        var facilitiesTable = document.getElementById("facilityTable");

        var facilityParams = document.getElementById("paramsTable");
        facilityParams.style.visibility = "visible";

        var title = document.getElementById("paramsTableTitle");
        title.style.captionSide = "top";
        title.style.textAlign = "center";
        title.innerHTML = facility.name.bold();
        title.style.border = "1px solid #E5E8E8";

        for (var i = 0; i < facility.params.length; i++)
        {
            console.log(i);
            var newRow = facilityParams.insertRow(facilityParams.rows.length);
            newRow.style.textAlign = "center";

            var name = newRow.insertCell(0)
            name.innerHTML = facility.params[i].name;

            var value = newRow.insertCell(1);
            value.innerHTML = facility.params[i].value;

            var metrics = newRow.insertCell(2);
            metrics.innerHTML = facility.params[i].metrics;
        }
    };
}