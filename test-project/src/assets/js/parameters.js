function renderParameters()
{
    // Тут костыль, пока только так.
    if (!this.calledBefore)
    {  
        this.calledBefore = true;
        
        var body = document.getElementById("canvas");
        body.style.position = "absolute";
        body.style.left = "0px";
        body.style.top = "0px";
        body.style.width = "100%";
        body.style.height = "100%";

        // Относительно таблицы считаются все остальные элементы
        var table = document.getElementById("facilityTable");        
        table.style.position = "absolute";
        table.style.left = "30%";
        table.style.top = String(window.innerHeight * 0.2) + "px";
        table.style.width = "40%";

        var editButton = document.getElementById("editButton");
        editButton.style.position = "absolute";
        editButton.style.top = String(table.offsetTop - document.getElementById("firstRow").offsetHeight) + "px";
        editButton.style.left = table.style.left;
        editButton.style.width = String(table.offsetWidth) + "px";
        editButton.style.height = String(document.getElementById("firstRow").offsetHeight) + "px";
        
        var editingButtonGroup = document.getElementById("editingButtonGroup");
        editingButtonGroup.style.position = "absolute";
        editingButtonGroup.style.left = String(table.offsetLeft + table.offsetWidth) + "px";
        editingButtonGroup.style.top = String(table.offsetTop + document.getElementById("firstRow").offsetHeight) + "px";

        var cancellingButtonGroup = document.getElementById("cancellingButtonGroup");
        cancellingButtonGroup.style.position = "absolute";
        cancellingButtonGroup.style.left = String(editingButtonGroup.offsetLeft + (table.offsetWidth / 20)) + "px";
        cancellingButtonGroup.style.top = String(table.offsetTop + document.getElementById("firstRow").offsetHeight) + "px";

        var addButton = document.getElementById("addButton");
        addButton.style.display = "none";
        addButton.style.position = "absolute";
        addButton.style.left = table.style.left;
        addButton.style.top = String(table.offsetTop + table.offsetHeight + 1) + "px";
        addButton.style.width = String(table.offsetWidth) + "px";
        addButton.style.height = String(document.getElementById("firstRow").offsetHeight) + "px";
    }
    else
    {
        // Rendering
        var table = document.getElementById("facilityTable");
        table.style.top = String(window.innerHeight * 0.2) + "px";

        var editButton = document.getElementById("editButton");
        editButton.style.height = String(document.getElementById("firstRow").offsetHeight) + "px";
        editButton.style.top = String(table.offsetTop - document.getElementById("firstRow").offsetHeight) + "px";
        editButton.style.width = String(table.offsetWidth) + "px";

        var addButton = document.getElementById("addButton");
        addButton.style.height = String(document.getElementById("firstRow").offsetHeight) + "px";
        addButton.style.top = String(table.offsetTop + table.offsetHeight) + "px";
        addButton.style.width = String(table.offsetWidth) + "px";

        var editingButtonGroup = document.getElementById("editingButtonGroup");
        editingButtonGroup.style.left = String(table.offsetLeft + table.offsetWidth) + "px";
        editingButtonGroup.style.top = String(table.offsetTop + document.getElementById("firstRow").offsetHeight) + "px";

        var cancellingButtonGroup = document.getElementById("cancellingButtonGroup");
        cancellingButtonGroup.style.left = String(editingButtonGroup.offsetLeft + (table.offsetWidth / 20)) + "px";
        cancellingButtonGroup.style.top = String(table.offsetTop + document.getElementById("firstRow").offsetHeight) + "px";

        for (var i = 0; i < editingButtonGroup.childElementCount; i++)
        {
            editingButtonGroup.childNodes[i].style.width = String(table.offsetWidth / 20) + "px";
            cancellingButtonGroup.childNodes[i].style.width = String(table.offsetWidth / 20) + "px";

            if (table.rows.length >= 2)
            {
                editingButtonGroup.childNodes[i].style.height = String(table.rows[1].offsetHeight + 1) + "px";
                cancellingButtonGroup.childNodes[i].style.height = String(table.rows[1].offsetHeight + 1) + "px";    
            }
        }

        var body = document.getElementById("canvas");

        if (addButton.offsetHeight + addButton.offsetTop > body.offsetHeight + body.offsetTop)
        {
            body.style.height = String(addButton.offsetHeight + addButton.offsetTop + 10) + "px";
        }
    }
}

function changeEditability()
{
    var editingButtonGroup = document.getElementById("editingButtonGroup");
    var cancellingButtonGroup = document.getElementById("cancellingButtonGroup");

    if (document.getElementById("addButton").style.display == "block")
    {
        document.getElementById("addButton").style.display = "none";

        for (var i = 0; i < editingButtonGroup.childElementCount; i++)
        {
            editingButtonGroup.childNodes[i].style.display = "none";
            cancellingButtonGroup.childNodes[i].style.display = "none";
        }
    }
    else
    {
        document.getElementById("addButton").style.display = "block";

        for (var i = 0; i < cancellingButtonGroup.childElementCount; i++)
        {
            editingButtonGroup.childNodes[i].style.display = "block";
            cancellingButtonGroup.childNodes[i].style.display = "block";
        }        
    }
}

function addFacility()
{
    var table = document.getElementById("facilityTable");
    var newRow = table.insertRow(table.rows.length);
    var facilityName = newRow.insertCell(0);
    var facilityParam = newRow.insertCell(1); 
    var paramUnit = newRow.insertCell(2);

    var newEditButton = document.createElement("button");
    newEditButton.className = "btn btn-light";
    newEditButton.style.width = String(Number(table.offsetWidth) / 20) + "px";
    newEditButton.style.height = String(Number(document.getElementById("firstRow").offsetHeight) + 1) + "px";
    newEditButton.style.backgroundColor = "green";
    newEditButton.style.borderColor = "green";
    newEditButton.style.padding = "0px 0px 0px 0px";
    newEditButton.style.textAlign = "center";
    newEditButton.innerHTML = "✎".bold();
    document.getElementById("editingButtonGroup").appendChild(newEditButton);

    var newCancelButton = document.createElement("button");
    newCancelButton.className = "btn btn-light";
    newCancelButton.style.width = String(Number(table.offsetWidth) / 20) + "px";
    newCancelButton.style.height = String(Number(document.getElementById("firstRow").offsetHeight) + 1) + "px";
    newCancelButton.style.backgroundColor = "red";
    newCancelButton.style.borderColor = "red";
    newCancelButton.style.padding = "0px 0px 0px 0px";
    newCancelButton.style.alignContent = "center";
    newCancelButton.innerHTML = "×".bold();
    document.getElementById("cancellingButtonGroup").appendChild(newCancelButton);

    facilityName.innerHTML = "new facility";
    facilityParam.innerHTML = "50";
    paramUnit.innerHTML = "м";

    var addButton = document.getElementById("addButton");
    addButton.style.top = String(Number(addButton.style.top.slice(0, -2)) + document.getElementById("firstRow").offsetHeight) + "px";
}